<?php
$controllers = array(
  'pages'   => ['home', 'single','error'],
  'login'   => ['showlogin','login','logout'],
  'signup'  => ['showsignup','signup'],
  'admin'   => ['showAddProDuct','addProduct'],
  'cart'    => ['addToCart','showCart']
); // Call controller

// show error when cannot call controller
if (!array_key_exists($controller, $controllers) || !in_array($action, $controllers[$controller])) {
  $controller = 'pages';
  $action = 'error';
}

// format controller
include_once('controllers/' . $controller . 'Controller.php');
// return to user
$klass = str_replace('_', '', ucwords($controller, '_')) . 'Controller';
$controller = new $klass;
$controller->$action();