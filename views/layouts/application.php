<html>
<head>
<title>Smart</title>
<link href="public/assets/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="public/assets/js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="public/assets/css/style.css" rel="stylesheet" type="text/css" media="all" />	
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Gaming Creators Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='//fonts.googleapis.com/css?family=Montserrat+Alternates:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
<!--//fonts-->
<script src="public/assets/js/modernizr.custom.js"></script>
	<link rel="stylesheet" type="text/css" href="public/assets/css/component.css" />
</head>
<body> 
<!--header-->	
<div class="header" >
	<div class="top-header" >		
		<div class="container">
		<div class="top-head" >	
				<div class="clearfix"> </div>
		</div>
		</div>
	</div>
		<!---->
	
		<div class="header-top">
		<div class="container">
		<div class="head-top">
		<div class="logo">
			
			<h1><a href="./index.php?controller=pages&action=home"><span> S</span>mart <span>O</span>sc</a></h1>
			
		</div>
		<div class="top-nav">		
			  <span class="menu"><img src="./public/assets/images/menu.png" alt=""> </span>
				
					<ul>
						<li><a class="color4" href="./index.php?controller=cart&action=showCart" id="cart-link" data-toggle="modal" data-target="#myModal">cart
							<?php
								if(isset($_SESSION['cart'])){?>
								<p id="session_cart"> <?php echo "(".$_SESSION['cart']['totalQty'].")";?></p>
									
								
							<?php }?>
						</a></li>
						<?php if(!isset($_SESSION['login'])){ ?>
						<li><a class="color5" href="./index.php?controller=signup&action=showsignup"  >SignUp</a></li>
						<li><a class="color6" href="./index.php?controller=login&action=showlogin" >Login</a></li>
						<?php }else {?>
							
							<?php if($_SESSION['login']['role']==1){?>
							<li><a class="color6" href="./index.php?controller=admin&action=showAddProDuct" ><?php echo $_SESSION['login']['user_name']?></a></li>
							<?php }else {?>
								<li><a class="color6" href="" ><?php echo $_SESSION['login']['user_name']?></a></li>
							<?php }?>
							<li><a class="color5" href="./index.php?controller=login&action=logout">Logout</a></li>
						<?php }?>
					</ul>
					<!--script-->
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(500, function(){
						});
					});
			</script>

				</div>
				
				<div class="clearfix"> </div>
		</div>
		</div>
	</div>
</div>
	<?= @$content ?>
	

<script src="public/assets/js/form/login.js"></script>
<script src="public/assets/js/form/signup.js"></script>
<script src="public/assets/js/form/fileUpload.js"></script>
<script src="public/assets/js/form/cart.js"></script>
</body>
</html>