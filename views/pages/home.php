<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
<title>ABC Store</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
body {font-family: "Lato", sans-serif}
.mySlides {display: none}
</style>
<body>
<!-- Navbar on small screens (remove the onclick attribute if you want the navbar to always show on top of the content when clicking on the links) -->


<!-- Page content -->
<div class="w3-content" style="max-width:2000px;margin-top:46px">

  <!-- Automatic Slideshow Images -->
  

  <!-- The Band Section -->
  <div class="w3-container w3-content w3-center w3-padding-64" style="max-width:800px" id="band">
    <h2 class="w3-wide">World Game</h2>
    <p class="w3-opacity"><i>We love Games</i></p>
  </div>

  <!-- The Tour Section -->
  <div class="w3-black" id="tour">
    <div class="w3-container w3-content w3-padding-64" style="max-width:800px">
      <h2 class="w3-wide w3-center">List Game</h2>
      <p class="w3-opacity w3-center"><i>Enjoy your choice!</i></p><br>
      <h2>List Game:</h2>
	
	  <?php while($row = mysqli_fetch_array($data)){ ?>
						<li style="margin-bottom: 40px;">
							<form  class="form-cart" data-id="<?php echo $row['id'] ?>">
							<input type="hidden" name="id"id="text<?php echo $row['id'] ?>" value="<?php echo $row['id'] ?>">
								<a href="./index.php?controller=pages&action=single&id=<?php echo $row['id'];?>" data-largesrc="./public/assets/images/<?php echo $row['image']; ?>" class="link-sg" data-linksg="" data-title="<?php echo $row['name'];?>" data-description="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque malesuada purus a convallis dictum. Phasellus sodales varius diam, non sagittis lectus. Morbi id magna ultricies ipsum condimentum scelerisque vel quis felis.. Donec et purus nec leo interdum sodales nec sit amet magna. Ut nec suscipit purus, quis viverra urna.">
								<div class="top">
								<img class="" src="<?php echo $row['image']; ?>" width="200px" height="200px" alt="img01"/>
								</div>
								<div class="bottom">
								<label style="font-size:20px"> <?php echo $row['name'];?></label>
								</div>
								</a>
								<div class="price">
									<label style="font-size:20px; color:white"><?php echo number_format($row['price']); ?> VND</label>
								</div>
								<div class="price">
									<button class="btn btn-warning" > Add to cart</button>
								</div>
							</form>
						</li>
						<?php } ?>
    </div>
  </div>

  <!-- Ticket Modal -->
  <div id="ticketModal" class="w3-modal">
    
  </div>

  <!-- The Contact Section -->
  <div class="w3-container w3-content w3-padding-64" style="max-width:800px" id="contact">
    <h2 class="w3-wide w3-center">CONTACT</h2>
    
  </div>
  
<!-- End Page Content -->
</div>


<!-- Footer -->
<footer class="w3-container w3-padding-64 w3-center w3-opacity w3-light-grey w3-xlarge">
  <i class="fa fa-facebook-official w3-hover-opacity"></i>
  <i class="fa fa-instagram w3-hover-opacity"></i>
  <i class="fa fa-snapchat w3-hover-opacity"></i>
  <i class="fa fa-pinterest-p w3-hover-opacity"></i>
  <i class="fa fa-twitter w3-hover-opacity"></i>
  <i class="fa fa-linkedin w3-hover-opacity"></i>
</footer>

<script>
// Automatic Slideshow - change image every 4 seconds
var myIndex = 0;
carousel();

function carousel() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  myIndex++;
  if (myIndex > x.length) {myIndex = 1}    
  x[myIndex-1].style.display = "block";  
  setTimeout(carousel, 4000);    
}

// Used to toggle the menu on small screens when clicking on the menu button
function myFunction() {
  var x = document.getElementById("navDemo");
  if (x.className.indexOf("w3-show") == -1) {
    x.className += " w3-show";
  } else { 
    x.className = x.className.replace(" w3-show", "");
  }
}

// When the user clicks anywhhttps://www.w3schools.com/w3css/tryit.asp?filename=tryw3css_templates_band&stacked=h#contactere outside of the modal, close it
var modal = document.getElementById('ticketModal');
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

</body>
</html>
