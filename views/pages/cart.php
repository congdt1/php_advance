<style>
   table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
  background-color: #4CAF50;
  color: white;
}
</style>
<h1>Your Cart</h1>
<table >
<tr >
<th >Name</th>
<th >Quantity</th>
<th >Price</th>
<th >Total price of game</th>
</tr>
<?php foreach($_SESSION['cart']['items'] as $key=>$cart){ ?>
<tr>
<td ><?php echo $cart['item']['name'] ?></td>
<td ><?php echo $cart['quantity'] ?></td>
<td ><?php echo $cart['item']['price'] ?></td>
<td ><?php echo $cart['price'] ?></td>
</tr>
<?php } ?>
</table>