<?php
class BaseController
{
  protected $folder; //Fodel u want to access
  //call model
  public function model($model){
    require_once "models/$model.php";
    return new $model;
  }
  // call view
  function render($file, $data = array())
  {
    // check file exists
    $view_file = 'views/' . $this->folder . '/' . $file . '.php';
    if (is_file($view_file)) {
      // if file exists the
      extract($data);
      ob_start();
      require_once($view_file);
      $content = ob_get_clean();
      require_once('views/layouts/application.php');
    } else {
      // return error page
      header('Location: index.php?controller=pages&action=error');
    }
  }
}