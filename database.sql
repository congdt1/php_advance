-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 20, 2020 at 10:14 AM
-- Server version: 8.0.21-0ubuntu0.20.04.4
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mvc_smart`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int UNSIGNED NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `price` int NOT NULL,
  `image` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `publisher` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci DEFAULT NULL,
  `quantity` int DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `image`, `publisher`, `quantity`, `created_at`, `updated_at`) VALUES
(7, 'ABC', 222, './public/assets/images/Screenshot from 2020-09-07 17-51-43.png', 'Riot', 2, '2020-09-16 08:49:03', '2020-09-16 08:49:03'),
(8, 'BCD', 222, './public/assets/images/Screenshot from 2020-09-07 17-51-43.png', 'Riot', 2, '2020-09-16 08:57:26', '2020-09-16 08:57:26'),
(9, 'DEF', 120000, './public/assets/images/Screenshot from 2020-09-07 17-51-43.png', 'Riot', 12, '2020-09-16 09:00:39', '2020-09-16 09:00:39'),
(10, 'FLI', 222, './public/assets/images/Screenshot from 2020-09-07 17-51-43.png', 'Riot', 2, '2020-09-20 02:47:49', '2020-09-20 02:47:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_vietnamese_ci NOT NULL,
  `role` int NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_vietnamese_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `role`, `created_at`, `updated_at`) VALUES
(4, 'cong', '$2y$10$i8/4nMVOPc4EAio.OviGYOafSnrn3BHi2VKMrnHge3jf7CeTcp6oy', 0, '2020-09-15 03:38:28', '2020-09-15 03:38:28'),
(10, 'asd', '$2y$10$w1W6TxyoJ2g0h.o2h523kOdUWomZUakPQgI3yZcQJfI6eukk6xxPy', 1, '2020-09-20 01:16:53', '2020-09-20 01:16:53'),
(12, 'cong123456', '$2y$10$DBoaJDpswAAVJj7.GP6VeOMfQE2eBxOzrJzw9vHZKucsdBKWGc5Wy', 1, '2020-09-20 01:43:47', '2020-09-20 01:43:47'),
(13, 'adminsmart', '$2y$10$umUA4qb4tY5CrEUnQODFd.XdA0Zo0ZYkpbcPmX2iqRRjYsPVsS0pO', 1, '2020-09-20 03:01:33', '2020-09-20 03:01:33'),
(14, 'Giang', '$2y$10$QRoSLJxZQG.NYk1M5HJff.bB4sblGm8XpAT8ul8oyQKBysoxvo2Be', 0, '2020-09-20 03:08:02', '2020-09-20 03:08:02'),
(15, 'giang1', '$2y$10$j4cBgKpbiNbCyijGVMfKKOVOTcvDFx8JZJ7Q2ezjTTuRmETNW4HRi', 0, '2020-09-20 03:09:46', '2020-09-20 03:09:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
